//***************************************************************************
// File name:  unittest1.cpp
// Author:     Chadd Williams
// Date:       4/22/2019
// Class:      CS485
// Purpose:    Demonstrate unit tests
//***************************************************************************

#include "stdafx.h"
#include "CppUnitTest.h"
#include "TicTacToeBoard.h"
#include <tchar.h>
#include <functional>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TIcTacToe_Model_UnitTest
{		
	TEST_CLASS(UnitTest1)
	{
  private:
    int mSharedState = 0;

    //***********************************************************************
    // Function:    updateAndPrintSharedState
    //
    // Description: adde 1 to mSharedState and log to the screen
    //
    // Parameters:  None
    //
    // Returned:    None
    //***********************************************************************

    void updateAndPrintSharedState ()
    {
      mSharedState++;
      Logger::WriteMessage ("mSharedState: ");
      Logger::WriteMessage (std::to_string (mSharedState).c_str ());

    }

	public:

    

    //***********************************************************************
    //
    // Description: Demonstrate initialization/cleanup functions
    //
    //***********************************************************************

    TEST_CLASS_INITIALIZE (classInit)
    {
      Logger::WriteMessage ("classInit ");

    }
    TEST_CLASS_CLEANUP (classCleanup)
    {
      Logger::WriteMessage ("classCleanup ");

    }

    TEST_METHOD_INITIALIZE (methodInit)
    {
      Logger::WriteMessage ("MethodInit ");

    }
    TEST_METHOD_CLEANUP (methodCleanup)
    {
      Logger::WriteMessage ("MethodCleanup ");

    }

    //***********************************************************************
    // Unit Test: FailingTest
    //
    // Description: This test should fail since the initial board is not full
    //
    //***********************************************************************
    TEST_METHOD (FailingTest)
    {
      TicTacToeBoard cTheBoard;
        

      updateAndPrintSharedState ();


      Assert::AreEqual (true, cTheBoard.isBoardFull (),
        _T("Failing Test Here is the msg!"), LINE_INFO ());

      Assert::IsTrue (cTheBoard.isBoardFull (), _T ("Failing Test"), 
        LINE_INFO ());
    }

    //***********************************************************************
    // Unit Test: makeMoveException
    //
    // Description: This test expects makeMove() to throw an exception
    // when the x or y coordinate is out of range.
    //
    //***********************************************************************
    TEST_METHOD (makeMoveException)
    {
      TicTacToeBoard cTheBoard;

      updateAndPrintSharedState ();

      Assert::ExpectException<std::range_error> (
        std::bind(&TicTacToeBoard::makeMove, &cTheBoard, 4, 0, 
          TicTacToeBoard::Player::ONE),
          _T("Exception Not Thrown"), LINE_INFO ());
    }


    //***********************************************************************
    // Unit Test: makeMoveUnexpectedExeception
    //
    // Description: This test should fail when an unexpected exception 
    // occurs.
    //
    //***********************************************************************
    TEST_METHOD (makeMoveUnexpectedException)
    {
      TicTacToeBoard cTheBoard;

      updateAndPrintSharedState ();

      Assert::IsTrue ( cTheBoard.makeMove(4,0,TicTacToeBoard::Player::ONE),
         _T ("make move failed!"), LINE_INFO ());
    }



    //***********************************************************************
    // Unit Test: TestMethod1
    //
    // Description: This test does nothing
    //
    //***********************************************************************
    TEST_METHOD (TestMethod1)
    {
      updateAndPrintSharedState ();
    }

	};
}